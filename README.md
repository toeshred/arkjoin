This script will alert an SMS email (or regular email) anytime a player connects
to your ARK server.

You can even exclude specific players within the script.


Download the script with git:

    git clone https://gitlab.com/toeshred/arkjoin.git

Make sure it is executable:

    cd arkjoin
    chmod +x arkjoin

Then run the script:

    ./arkjoin &disown

Stopping the script can be done by killing the process with `pkill arkjoin`.

This does require the email program `mutt` to be configured.

To have the script run automatically at boot time, add the following in cron:

    @reboot /path/to/arkjoin
